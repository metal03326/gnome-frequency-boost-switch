# Frequency Boost Switch

GNOME extension that adds a toggle to enable/disable CPU frequency boost in Gnome system menu.

<img src="imgs/screenshots/clean.png" alt="drawing" width="402"/>
<img src="imgs/screenshots/extension.png" alt="drawing" width="402"/>
<img src="imgs/screenshots/timed.png" alt="drawing" width="402"/><br/>
<img src="imgs/screenshots/general.png" alt="drawing" width="640"/>
<img src="imgs/screenshots/intel.png" alt="drawing" width="672"/>
<img src="imgs/screenshots/debug.png" alt="drawing" width="672"/>

## Main features

- Toggle Frequency Boost from the same place you set power profile
- Toggle Frequency Boost regardless of the set power profile
- Lightweight (no service is installed) <sup>1</sup> <sup>3</sup>
- Option to set on boot (Persist) <sup>2</sup> <sup>3</sup>
- Option to switch state for a period of time only <sup>4</sup> <sup>5</sup>
- Option to set Intel Energy-Performance Preference (EPP) as well
- Option to set Intel Performance and Energy Bias Hint (EPB) as well
- Option to cleanup the user interface by removing toggle submenu and subtitle
- Option to toggle the power profile as well <sup>6</sup>
- Translation (currently only English and Bulgarian)

<sup>1</sup> Asks for permission every time user flips the switch<br/>
<sup>2</sup> Will prompt user for permission 5s after login, if boost needs to be changed<br/>
<sup>3</sup> Permission prompt can be removed by adding Polkit rules. Check extension preferences.<br/>
<sup>4</sup> For now only 1, 2, 5, 12, or 24 hour periods are supported (2 and 5
since [v19](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v19))<br/>
<sup>5</sup> GNOME disables extensions when the user locks the PC. This means no switching can be done while the PC is
locked. Once the user unlocks it, events will take place as if it were never locked: boost will flip if the time has
passed, or a new timeout will commence with the remaining time. There is a way to go around this,
check [Adding the toggle to the lock screen](#adding-the-toggle-to-the-lock-screen).<br/>
<sup>6</sup> Available only if `power-profiles-daemon` or `TuneD` is installed.

## Requirements

- pkexec (needed to ask the user for permissions), preferably >= 0.106
- GNOME >= 45 (check table below for older versions)

Main testing (and development) happens on the latest Fedora. I also have set up the latest Ubuntu and Pop!_OS on 2 other
machines just to test the extension.

## Compatibility

This extension doesn't change much, so having a clean code path is of a high priority. Thus, some changes on Gnome side
will result in backwards incompatibility. Not that I cannot make it work, but I would prefer less code (which results in
less bugs). Older Gnome versions will still function with an older version of the extension. The main functionality is
still the same, even in v1.

| GNOME Version | Last extension version to support it                                             | Reason for dropping                       |
|---------------|----------------------------------------------------------------------------------|-------------------------------------------|
| 41            | [v6](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v6)   | New Preferences dialog with Adwaita theme |
| 42            | [v8](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v8)   | Gnome 43 redesigned the menus             |
| 43            | [v9](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v9)   | Gnome 44 changed how quick toggles work   |
| 44            | [v12](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v12) | Gnome 45 changed the way imports work     |
| 45, 46, 47    | Latest                                                                           |                                           |

## Supported hardware

Typically, all CPUs with Intel Turbo Boost, AMD Turbo Core and AMD Precision Boost technologies should work. I've tested
on the following:

- **intel_pstate** (tested on **11th Gen Intel® Core™ i7-1165G7**)
- **intel_cpufreq** (tested on **4th Gen Intel® Core™ i7-4500U**)
- **acpi-cpufreq** (tested on an old **AMD® PRO A4-3350B**)
- **amd-pstate** (**partially**, check the
  pending [issue](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/issues/5))

## Why the need

There are many tools out there that offer **automatic** power management, but they all fail in one simple scenario -
running off of a **power bank**. To them, the computer is running on AC, which is not true and there is no way they can
know that. So **manual** setting is the only solution. I use the GNOME way to manually switch the power profile, but
what I was missing is the option to easily control the Frequency Boost, hence this extension was born.

## Why tweaking Intel EPP and EPB

Same reason - **power banks**. Consumed power to do a specific job is relatively the same between doing it faster using
more peak power or slower using less peak power. When you run off of a **20W** PD power bank, every time your total
consumption goes above 20W, your internal battery will need to help, draining it in the process. With Intel EPB and EPP
you can make the CPU stay even longer in a deeper C state. The end result is relatively the same energy is used, but
more of that energy will come out of the external 20W power bank and less from the internal battery.

## Compatibility

If you run another software that also sets the Frequency Boost, they will collide. I get the current boost state when
the user opens the Power Profiles menu, so it should reflect the real state, but I cannot prevent other software from
also setting the state.

## Adding the toggle to the lock screen

**Warning!** This is not advisable. Use at your own risk.

If you add the toggle to the lock screen, you gain two things:

- Properly working timers for the timed options
- Ability to toggle the boost on the lock screen

Adding it is easy, but first you need to ensure you have the Polkit rules added and working, as there is no way to ask
for permissions on the lock screen. This is the main reason why this is not a baked-in feature.

If you are sure your toggle doesn't ask for a password, then open `metadata.json` and
add `"session-modes": ["user", "unlock-dialog"],`, then restart the session (logout and then login, or restart). An easy
way to find the `metadata.json` is by going to the Preferences of the extension ⇾ Debug tab and click the **Locate**
button for the set_boost. `metadata.json` is in the same folder as `set_boost`.

Keep in mind `metadata.json` is replaced on each extension update, so you will need to do that periodically.

I've been using this since [v15](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v15), but it
should be ok (I haven't tested) going back
to [v12](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v12) (included).

## Installation

### From GNOME Extensions Website

[![Frequency Boost Switch on extensions.gnome.org](imgs/ego.svg)](https://extensions.gnome.org/extension/4792/frequency-boost-switch/)

### From source

```
git clone https://gitlab.com/metal03326/gnome-frequency-boost-switch.git
cd gnome-frequency-boost-switch
make
```

## FAQ

### Q: Switch not appearing

A: Most likely, CPU is not supported. Check the preferences of the extension - it'll tell you if that is the case.

### Q: Switch appears with delay

A: This is on purpose - 5 seconds after first login are required for both the switch to appear and persistent boost
setting to take place

### Q: Toggling the switch doesn't work

A: Make sure `~/.local/share/gnome-shell/extensions/freq-boost-switch@metal03326/set_boost` is executable. Also, if
using Intel EPP or EPB, try setting both of them to No Change to eliminate that as an issue.

### Q: Toggling the switch doesn't toggle the power profile even though I set it in the preferences

A: There is currently a [bug](https://github.com/redhat-performance/tuned/issues/689) opened about that on the TuneD repo. The issue is purely visual. Once fixed, it should start working.

### Q: Polkit rules added, but permission dialog still shows

A: Rules are added under `/etc/polkit-1/rules.d/` or `/usr/share/polkit-1/rules.d` (as of
[v15](https://gitlab.com/metal03326/gnome-frequency-boost-switch/-/releases/v15)), which may not be the place for all
distros. Check if your distro uses one of those paths and if not, copy `freq-boost-switch.rules` to the appropriate
folder.

### Q: Does it work under Wayland

A: Yes. It actually doesn't have anything to do with display server. X11 or Wayland doesn't matter.

## Troubleshooting

If all else fails, check logs for errors:

`journalctl /usr/bin/gnome-shell -f -o cat` for the extension itself.

`journalctl -o cat -f /usr/bin/gjs` for the preferences window.

## Contribution

You are always welcome to open issues and requests ;)

## License

[GPLv3](https://www.gnu.org/licenses/gpl.html)
