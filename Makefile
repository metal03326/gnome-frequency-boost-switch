all: build install

build:
	gnome-extensions pack --extra-source=set_boost --extra-source=general.ui --extra-source=intel.ui --extra-source=debug.ui --extra-source=freq-boost-switch.rules --extra-source=common.js --out-dir=. --podir=po --schema=schemas/org.gnome.shell.extensions.freqboostswitch.gschema.xml --force src

install:
	gnome-extensions install --force ./freq-boost-switch\@metal03326.shell-extension.zip

gettext:
	xgettext --output=src/po/texts.pot src/*.js src/*.ui

settext:
	msginit --locale bg --input src/po/texts.pot --output src/po/bg.po